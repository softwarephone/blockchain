const WebSocket = require('ws');

const P2P_PORT = process.env.P2P_PORT || 5001;
const peers = process.env.PEERS ? process.env.PEERS.split(',') : [];
const MESSAGES_TYPES = {
    chain : 'CHAIN',
    transaction : 'TRANSACTION',
    clear_transactions : 'CLEAR_TRANSACTIONS'
};


class P2pServer {
    constructor(blockchain, ttransactionPool) {
        this.blockchain = blockchain;
        this.transactionPool = ttransactionPool
        this.sockets = [];
    }

    listen() {
        const server = new WebSocket.Server({ port: P2P_PORT });
        server.on('connection', socket => this.connectSocket(socket));

        this.connectToPeers();
        console.log(`Listening for peer to peer connections on : ${P2P_PORT}`)
    }

    connectToPeers() {
        peers.forEach(peer => {
            const sockect = new WebSocket(peer);
            sockect.on('open', () => this.connectSocket(sockect))
        })
    }

    connectSocket(sockect) {
        this.sockets.push(sockect);
        console.log('socket connected');
        this.messageHandler(sockect);
        this.sendChain(sockect)
    }

    messageHandler(socket) {
        socket.on('message', message => {
           const data = JSON.parse(message);
           if (data.type === MESSAGES_TYPES.chain) {
               this.blockchain.replaceChain(data.chain)
           } else if (data.type === MESSAGES_TYPES.transaction) {
               this.transactionPool.updateOrAddTransaction(data.transaction)
           } else if (data.type === MESSAGES_TYPES.clear_transactions) {
               this.transactionPool.clear();
           }
        });
    }

    sendChain(socket) {
        socket.send(JSON.stringify( { type : MESSAGES_TYPES.chain , chain: this.blockchain.chain } ));
    }

    sendTransaction(socket, transaction) {
        socket.send(JSON.stringify({ type : MESSAGES_TYPES.transaction , transaction }));
    }

    syncChains() {
        this.sockets.forEach(socket => this.sendChain(socket))
    }


    broadcastTransaction(transaction) {
        this.sockets.forEach(socket => this.sendTransaction(socket, transaction))
    }


    broadcastClearTransactions() {
        this.sockets.forEach(socket => socket.send(JSON.stringify({ "type" : MESSAGES_TYPES.clear_transactions})))
    }
}


module.exports = P2pServer;