const Transaction = require('../wallet/transaction');
const TransactionPool = require('../wallet/transaction-pool');
const Wallet = require('../wallet');

class Miner {
    constructor(blockchain, transactionPool, wallet, p2pServer) {
        this.blockchain = blockchain;
        this.transactionPool = transactionPool;
        this.wallet = wallet;
        this.p2pserver = p2pServer;
    }

    mine() {
        const validTransactions = this.transactionPool.validTransactions();
        validTransactions.push(Transaction.rewardTransaction(this.wallet, Wallet.blockChainWallet()));
        const block = this.blockchain.addBlock(validTransactions);
        this.p2pserver.syncChains();
        this.transactionPool.clear();
        this.p2pserver.broadcastClearTransactions();
        return block;
    }
}


module.exports = Miner;