const { INITIAL_BALANCE } = require('../config');
const Transaction = require('./transaction');

const ChainUtil = require('../chain-util');

class Wallet {
    constructor() {
        this.balance = INITIAL_BALANCE;
        this.keyPair = ChainUtil.genKeyPair();
        this.publicKey = this.keyPair.getPublic().encode('hex');
    }

    toString() {
        return ` Wallet - 
            publicKey: ${this.publicKey.toString()}
            balance: ${this.balance}`;
    }

    sign(dataHash) {
        return this.keyPair.sign(ChainUtil.hash(dataHash));
    }

    createTransaction(recipient, amount, blockchain, transactionPool) {
        this.balance = this.calculateBalance(blockchain);

        if (amount > this.balance)  {
            console.log(`Amount : ${amount} exceeds the balance ${this.balance}`);
            return;
        }
        let t = transactionPool.existingTransaction(this.publicKey);
        if (t) {
            t.update(this, recipient, amount)
        } else {
            t = Transaction.newTransaction(this, recipient, amount);
            transactionPool.updateOrAddTransaction(t)
        }
        return t;
    }

    calculateBalance(blockchain) {
        let balance = this.balance;
        let transactions = [];

        blockchain.chain.forEach(block => block.data.forEach(t => {
            transactions.push(t)
        }));

        const walletInputTs = transactions.filter(t => t.input.address === this.publicKey);

        let startTime = 0;

        if (walletInputTs.length > 0) {
            const recentInputT = walletInputTs.reduce((prev, current) => prev.input.timestamp > current.input.timestamp ? prev : current);

            balance = recentInputT.outputs.find(out => out.address === this.publicKey).amount;
            startTime = recentInputT.input.timestamp
        }

        transactions.forEach(transaction => {
            if (transaction.input.timestamp > startTime) {
                transaction.outputs.find(o => {
                    if(o.address === this.publicKey) {
                        balance += o.amount
                    }
                })
            }
        });

        return balance;
    }

    static blockChainWallet() {
        const bcw = new this();
        bcw.address = 'blockchain-wallet';
        return bcw;
    }
}


module.exports = Wallet;