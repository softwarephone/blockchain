const Transaction = require('./transaction');

class TransactionPool {
    constructor() {
        this.transactions = [];
    }

    updateOrAddTransaction(transaction) {
        let transationWithId = this.transactions.find(t => t.id === transaction.id);
        if(transationWithId) {
            this.transactions[this.transactions.indexOf(transationWithId)] = transaction;
        } else  {
            this.transactions.push(transaction);
        }
    }

    existingTransaction(address) {
        return this.transactions.find(t => t.input.address === address);
    }

    validTransactions() {
        return this.transactions.filter(t => {
            const outputTotal = t.outputs.reduce((total, out) => {
                return total + out.amount
            }, 0);
            
            if (t.input.amount !== outputTotal){
                console.log(`Invalid transaction from ${t.input.address}`);
                return;
            }

            if(!Transaction.verifyTransaction(t)) {
                console.log(`Invalid signature from ${t.input.address}`);
                return;
            }

            return t
        })
    }

    clear() {
        this.transactions = []
    }
}


module.exports = TransactionPool;