const Transaction = require('./transaction');
const Wallet = require('./index');
const {MINING_REWARD} = require('../config');


describe('Transaction', () => {
   let transaction, wallet, amount, recipient;

    beforeEach(()=> {
       wallet = new Wallet();
       amount = 50;
       recipient = 'recipient';
       transaction = Transaction.newTransaction(wallet, recipient, amount)
   });


    it('should outputs the amount subtracted from the wallet balance', function () {
        expect(transaction.outputs.find(output => output.address === wallet.publicKey).amount)
            .toEqual(wallet.balance - amount);
    });

    it('should outputs amount add to the recipient', function () {
        expect(transaction.outputs.find(output => output.address === recipient).amount)
            .toEqual(amount)
    });


    it('should inputs the balance of the wallet', function () {
        expect(transaction.input.amount).toEqual(wallet.balance);
    });

    it('should validate a valid transaction', function () {
        expect(Transaction.verifyTransaction(transaction)).toBe(true);
    });


    it('should invalidate a corupt transaction ', function () {
        transaction.outputs[0].amount = 5000;
        expect(Transaction.verifyTransaction(transaction)).toBe(false);
    });

    describe('transaction with an amount that exceeds balance', () => {

        beforeEach(() => {
            amount = 500000;
            transaction = Transaction.newTransaction(wallet, recipient, amount)
        });

        it('should does not create transaction', function () {
            expect(transaction).toEqual(undefined)

        });



    });

    describe('transaction update ', () => {
        let nextAmount, nextRecipient;

        beforeEach(() => {
            nextAmount = 20;
            nextRecipient = 'next-address';
            transaction = transaction.update(wallet, nextRecipient, nextAmount)
        });

        it('should does subtract the next amount', function () {
            expect(transaction.outputs.find(out => out.address === wallet.publicKey).amount).toEqual(wallet.balance - amount - nextAmount);
        });

        it('should outputs an amount for the next recipient', function () {
            expect(transaction.outputs.find(out => out.address === nextRecipient).amount).toEqual(nextAmount)
        });
    });



    describe('creating and reward transaction', () => {

        beforeEach(() => {
            transaction = Transaction.rewardTransaction(wallet, Wallet.blockChainWallet())
        });


        it('should reward the miner', function () {
            expect(transaction.outputs.find(out => out.address === wallet.publicKey).amount).toEqual(MINING_REWARD)
        });
    })

});