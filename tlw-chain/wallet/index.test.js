const Wallet = require('./index');
const TransactionPool = require('./transaction-pool');
const Blockchain = require('../blockchain');
const { INITIAL_BALANCE } = require('../config');

describe('Wallet', () => {
   let wallet, tp, bc;

   beforeEach(()=> {
       wallet = new Wallet();
       tp = new TransactionPool();
       bc = new Blockchain();
   });


    describe('create a transaction', () => {
        let transaction, sendAmount, recipient;


        beforeEach(()=> {
            sendAmount = 50;
            recipient = 'radanm-32RDRF';
            transaction = wallet.createTransaction(recipient, sendAmount, bc, tp)
        });

        describe('and doing the same trasanction', () => {
            beforeEach(()=> {
                transaction = wallet.createTransaction(recipient, sendAmount, bc, tp)
            });

            it('should double the sendAmount substracted from the wallet balance ', function () {
                expect(transaction.outputs.find(out => out.address === wallet.publicKey).amount).toEqual(wallet.balance - (sendAmount * 2));
            });

            it('should clones the sendAmount output for the recipient ', function () {
                expect(transaction.outputs.filter(out => out.address === recipient).map(out => out.amount)).toEqual([sendAmount,sendAmount]);
            });
        });
    });



    describe('calclating balance', () => {
        let addBalance, repeadAdd, senderWallet;


        beforeEach(() => {
            senderWallet = new Wallet();
            addBalance = 100;
            repeadAdd = 3;
            for (let i = 0; i < repeadAdd ; i++) {
                senderWallet.createTransaction(wallet.publicKey, addBalance, bc, tp);
            }
            bc.addBlock(tp.transactions);
        });


        it('should calculte the balance for blockchain matching transaction', function () {
            expect(wallet.calculateBalance(bc)).toEqual(INITIAL_BALANCE  + (addBalance * repeadAdd))
        });

        it('should calculte the balance for blockchain matching transaction', function () {
            expect(senderWallet.calculateBalance(bc)).toEqual(INITIAL_BALANCE  - (addBalance * repeadAdd))
        });


        describe(' and the receipient conducts a transaction', () => {
            let subtractBalance, recipientBalance;


            beforeEach(()=> {
                subtractBalance = 60;
                recipientBalance = wallet.calculateBalance(bc);
                wallet.createTransaction(senderWallet.publicKey, subtractBalance, bc, tp);
                bc.addBlock(tp.transactions)
            });


            describe(' and the sender sends another transaction to the recipient', () => {
                beforeEach(()=> {
                    tp.clear();
                    senderWallet.createTransaction(wallet.publicKey, addBalance, bc, tp);
                    bc.addBlock(tp.transactions)
                });

                it('should calculate the recipient balance only using trasanction since its most recent one', function () {
                    expect(wallet.calculateBalance(bc)).toEqual(recipientBalance - subtractBalance + addBalance)
                });
            })

        })
    });

});