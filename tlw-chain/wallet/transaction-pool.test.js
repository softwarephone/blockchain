const TransactionPool = require('./transaction-pool');
const Transaction = require('./transaction');
const Wallet = require('./index');
const Blockchain = require('../blockchain');


describe('TransactionPoll', () => {
    let tp, wallet, transaction, bc;
    beforeEach(()=> {
        tp = new TransactionPool();
        wallet = new Wallet();
        bc = new Blockchain();
        transaction = wallet.createTransaction('r4and-Adress', 30, bc, tp)
    });


    it('should add the transaction to the pool', function () {
        expect(tp.transactions.find(t => t.id === transaction.id)).toEqual(transaction);
    });


    it('should update a transaction in the pool', function () {
        const oldTransaction = JSON.stringify(transaction);
        const newTransaction = transaction.update(wallet, 'foo-4ddr355', 40);
        tp.updateOrAddTransaction(newTransaction);
        expect(JSON.stringify(tp.transactions.find(t => t.id))).not.toEqual(oldTransaction)
    });


    it('should clear the transaxction tool', function () {
        tp.clear();
        expect(tp.transactions).toEqual([])
    });

    describe('Mixing valid and corrupt transaction', () => {
        let validTransactions ;

        beforeEach(()=> {
            validTransactions = [...tp.transactions];
            for (let i = 0; i < 6; i++ ) {
                wallet = new Wallet();
                transaction = wallet.createTransaction('random-addres',30, bc, tp);
                if(i % 2 === 0) {
                    transaction.input.amount = 9999;
                } else {
                    validTransactions.push(transaction)
                }
            }
        });


        it('should detects corrupts transaction from bad transaction', function () {
            expect(JSON.stringify(tp.transactions)).not.toEqual(JSON.stringify(validTransactions))
        });

        it('should grabs valid transaction from bad transaction', function () {
            expect(tp.validTransactions()).toEqual(validTransactions)
        });
    })

});