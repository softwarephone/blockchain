const ChainUtil = require('../chain-util');
const { DIFFICULTY, DEFAULT_NONCE, MIN_RATE } = require('../config');

class Block {
    constructor(timestamp, lastHash, hash, data, nonce, difficulty) {
        this.timestamp = timestamp;
        this.lastHash = lastHash;
        this.hash = hash;
        this.data = data;
        this.nonce = nonce;
        this.difficulty = difficulty || DIFFICULTY;
    }
    toString() {
        return `Block -
          Timestamp : ${this.timestamp}
          Last Hash : ${this.lastHash.substring(0, 10)}
          Hash      : ${this.hash.substring(0, 10)}
          Nonce     : ${this.nonce}
          Difficulty: ${this.difficulty}
          Data      : ${this.data}`;
    }

    static genesis() {
        return new this('Genesis Time', '-------------------', 'f1rst-H85h', [], DEFAULT_NONCE, DIFFICULTY)
    }

    static mineBlock(lashBlock, data) {
        let hash, timestamp;
        const lastHash = lashBlock.hash;
        let nonce = 0;
        let { difficulty } = lashBlock.difficulty;
        do {
            nonce++;
            timestamp = Date.now();
            difficulty = Block.ajdustDifficulty(lashBlock, timestamp);
            hash = Block.hash(timestamp, lastHash, data, nonce, difficulty);
        } while (hash.substring(0, difficulty) !== '0'.repeat(difficulty));

        return new this(timestamp, lashBlock.hash, hash, data, nonce, difficulty)
    }

    static hash(timestamp, lastHash, data, nonce, difficulty) {
        return ChainUtil.hash(`${timestamp}${lastHash}${data}${nonce}${difficulty}`)
    }

    static ajdustDifficulty(lastBlock, currentTime) {
        let { difficulty } = lastBlock;
        difficulty = lastBlock.timestamp + MIN_RATE > currentTime ? difficulty + 1 : difficulty - 1;
        return difficulty;
    }

    static blockHash(block) {
        const {timestamp, lastHash, data, nonce, difficulty } = block;
        return Block.hash(timestamp,lastHash, data, nonce, difficulty)
    }
}

module.exports = Block;
