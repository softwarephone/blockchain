const Blockchain = require('./index');
const Block = require('./block');


describe('Blockchain', () => {

    let blockchain, blockchain2;

    beforeEach(() => {
        blockchain = new Blockchain();
        blockchain2 = new Blockchain();
    });

    it('should starts with genesis', function () {
        expect(blockchain.chain[0]).toEqual(Block.genesis());
    });


    it('should adds a new block', function () {
        const data = 'foo';
        blockchain.addBlock(data);

        expect(blockchain.chain[blockchain.chain.length - 1].data).toEqual(data)
    });

    it('should validates a valid chain', function () {
        blockchain2.addBlock('foo');
        expect(blockchain.isValidChain(blockchain2.chain)).toBe(true);
    });

    it('should invalidate the chain with genesis', function () {
        blockchain2.chain[0].data = 'Bad data';
        expect(blockchain.isValidChain(blockchain2.chain)).toBe(false)
    });

    it('should invalidates a corrupt chain', function () {
        blockchain2.addBlock('foo');
        blockchain2.chain[1].data = 'Not foo';
        expect(blockchain.isValidChain(blockchain2.chain)).toBe(false)
    });

    it('should replace a chain whith a valid chain', function () {
        blockchain2.addBlock('enother');
        blockchain.replaceChain(blockchain2.chain);

        expect(blockchain.chain).toEqual(blockchain2.chain)
    });

    it('should not replace the chain with one les than or equal to lenght', function () {
        blockchain2.addBlock("Seconed block");
        blockchain2.replaceChain(blockchain.chain);
        expect(blockchain.chain).not.toEqual(blockchain2.chain)
    });
});