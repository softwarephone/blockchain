const Block = require('./block');

describe('Block', () => {
    let data, lastBlock, block;

    beforeEach(() => {
        data = 'the data';
        lastBlock = Block.genesis();
        block = Block.mineBlock(lastBlock, data)

    });

    it('should sets the `data` to match the input', function () {
        expect(block.data).toEqual(data);
    });

    it('should set the lastHash', function () {
        expect(block.lastHash).toEqual(lastBlock.hash)
    });

    it('should contains at least 0 of difficulty', function () {
        expect(block.hash.substring(0,block.difficulty)).toEqual('0'.repeat(block.difficulty))
    });


    it('should lowers the difficulty for slowly mined blocks', function () {
        expect(Block.ajdustDifficulty(block, block.timestamp + 360000)).toEqual(block.difficulty-1)
    });


    it('should raises the difficulty for quickly mined blocks', function () {
        expect(Block.ajdustDifficulty(block, block.timestamp + 1)).toEqual(block.difficulty+1)
    });

});